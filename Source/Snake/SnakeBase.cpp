// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	MovementSpeed = 10.f;
	LastMoveDirection = EMovementDirection::UP;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(4);
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	Move();
}

void ASnakeBase::AddSnakeElement(int32 ElementsNum)
{
	for (int32 i = 0; i < ElementsNum; i++)
	{
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, FTransform(GetActorLocation() - FVector(SnakeElements.Num() * ElementSize, 0, 0)));
		int32 ElemIndex = SnakeElements.Add(NewSnakeElem);
		NewSnakeElem->SnakeOwner = this;
		
		if (!ElemIndex)
			NewSnakeElem->SetFirstElementType();
	}
}

void ASnakeBase::Move()
{
	FVector MovementVector;

	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += ElementSize;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= ElementSize;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += ElementSize;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= ElementSize;
		break;
	}

	//for (int32 i = 0; i < SnakeElements.Num(); i++)
	//	SnakeElements[i]->ToggleCollision();
	SnakeElements[0]->ToggleCollision();
	
	for (int32 i = SnakeElements.Num() - 1; i > 0; i--)
		SnakeElements[i]->SetActorLocation(SnakeElements[i - 1]->GetActorLocation());
	SnakeElements[0]->AddActorWorldOffset(MovementVector);

	//for (int32 i = 0; i < SnakeElements.Num(); i++)
		SnakeElements[0]->ToggleCollision();

}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex = 0;
		SnakeElements.Find(OverlappedElement, ElemIndex);

		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface)
			InteractableInterface->Interact(this, ElemIndex == 0);
	}
}

